"""" This project is a part of Question Answering project on "Der Spiegel" articles in the context
    of Text mining Exercises, Winter Semester 2016/17

    Authors: Amani Ben Hadj Hassen : amanibelhajhsan@gmail.com
             Abdesslem Maktouf :  maktouf.abdessalem@gmail.com

    The goal is to extract Object - predicate - subject from Der Spiegel articles in the german language.
    Project pipeline : coreference resolution => named entity recognition => open relation extraction
    => Saving in an RDF style
 """

import tools
import coref_res
import spacy

nlp = spacy.load('de')

# open the file containing the article in question
print("***** Beginning *****")
my_file = open('exp_article2', 'r')
print("***** file read *****")

# perform coreference resolution, my_coref = [] of (name, index) pairs
my_coref = coref_res.coreference_resolution(my_file)
# print(my_coref)

# replace the pronouns with th22e adequate name
my_list = []
my_file = open('exp_article2', 'r')
my_sentences = tools.loadfile(my_file)
nlp_sentences = tools.my_nlp(my_sentences)
for elem in nlp_sentences:
    for word in elem:
        my_list.append(word)

for element in my_coref:
    # print(element)
    my_list[element[1]] = element[0]

for i, word in enumerate(my_list):
    if type(my_list[i]) == str:
        my_list[i] = my_list[i]
    else:
        my_list[i] = my_list[i].text
text_treated = ' '.join(my_list)
print(text_treated)

out = open("article2.txt", "w")
out.write(text_treated + '\n')
print("***** The end ! *****")
