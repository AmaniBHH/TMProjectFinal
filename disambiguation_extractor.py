import os
import re
import tools

# generate the path to the file to be treated
script_dir = os.path.dirname(__file__)    # <-- absolute dir the script is in
rel_path = "DBpediaFiles/disambiguations_de.ttl"
abs_file_path = os.path.join(script_dir, rel_path)

# create a file to save the urls in
disambiguations_file = open('DBpediaFiles/disambiguations.csv', 'w')

disambiguations = []
with open(abs_file_path, 'r') as f:    # open the file
    contents = tools.loadfile(f)    # put the lines to a variable.
    length = len(contents)
    L = []
    line = contents[1].split()
    L.append(line[2])
    my_url = line[0]
    for index in range(2, 40):
        next_line = contents[index].split()
        """print("***************", index, "***************")
        print("line :", line[0])
        print("next line :", next_line[0])"""
        if next_line[0] == my_url:
            L.append(next_line[2])
        else:
            disambiguations.append([my_url, L])
            L = []
            line = contents[index].split()
            L.append(line[2])
            my_url = line[0]
        # print("L :", L)

    disambiguations.append([my_url, L])


for elem in disambiguations:
    disambiguations_file.write(elem[0])
    disambiguations_file.write(';')
    for l in elem[1]:
        disambiguations_file.write(l)
        disambiguations_file.write(';')
    disambiguations_file.write('\n')

