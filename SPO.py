import os
import tools
from pathlib import Path
import spacy

nlp = spacy.load("de")
OBJECTDEP = ["oa", "oa2", "og", "oc", "op"]


def splitsent(sentence):
    return [x.strip ( ) for x in sentence.split ( ',' )]


def getroot(sentence):
    # return the root of the sentence
    for token in sentence:
        if token.head is token:
            return token.text


def getmainsent(sentence):
    for ind in splitsent(sentence.text):
        if getroot(sentence) in ind:
            return ind



def getrootposition(sentence):
    # return the root of the sentence
    for token in sentence:
        if token.head is token:
            return token.i


def getkouspos(sentence):
    # return a dictionary with the conjunction indication and its position
    # return the position of the conjunction indication
    position = ""
    for token in sentence:
        if token.tag_ == "KOUS":
            position = token.i
            # position.update({token.text : token.i})
    return position


def splitsentence(sentence):
    result = {}
    for token in sentence:
        if (token.tag_ == "KOUS") and (sentence[token.i-1].text == ","):
            result = {"sentence": sentence.text,
            "clauses": {"main": sentence[0: token.i-1].text,
             "second": sentence[token.i+1: -1].text},
                     "Context": contextsentence(sentence)}
        elif token.text == "," and sentence[token.i+1].pos_ in ["AUX","VERB"]:
            result = {"sentence": sentence.text,
                      "clauses": {"main": sentence[token.i + 1: -1].text,
                                  "second":  sentence[0: token.i].text},
                      "Context": contextsentence(sentence)}
        else:
            pass
    if not result:
        result = {"sentence": sentence.text}
    return result


def removecontextfromsentence(sentence):
    sentencelist = [x.strip() for x in sentence.split(',')]
    listfreecontext = []
    for sent in sentencelist:
        sent = nlp(sent)
        contains = False
        for token in sent:
            if token.pos_ in ["AUX", "VERB"]:
                contains = True
        if contains:
            listfreecontext.append(sent.text)
    sentence_free = ' '.join(listfreecontext)
    return sentence_free


def contextsentence(sentence):
    context = {}
    numb = 1
    sentencelist = [x.strip() for x in sentence.text.split(',')]
    for sent in sentencelist:
        sent = nlp(sent)
        contains = False
        for token in sent:
            if token.pos_ in ["AUX", "VERB"]:
                contains = True
        if contains == False:
            context.update({numb: sent.text})
            numb += 1
    return context


def getsubject(sentence):
    if type(sentence) == str:
        sentence = nlp(sentence)
    subjects = []
    rootpos = getrootposition(sentence)
    # subjects.append ( [token for token in sentence if token.dep_ == "sb" or (token.dep_ == "cj" and token.pos_ != "NOUN" and token.pos_ != "VERB" and token.pos_ == "PRON")] )
    for token in sentence:
        if (token.dep_ == "sb" or (token.dep_ == "cj" and token.pos_ != "NOUN" and token.pos_ != "VERB" and token.pos_ == "PRON")) and token.i < rootpos :
            if sentence[token.i-1].dep_ == "nk" and sentence[token.i-1].tag_ == "ART":
                subjects.append(sentence[token.i-1].text + " " + token.text)
            else:
                subjects.append(token.text)
        if (token.text == "und") and token.i < rootpos and (sentence[token.i+1].dep_ == "nk") and (sentence[token.i+1].tag_ == "ART") and ((sentence[token.i+2].dep_ == "cj") and (sentence[token.i+2].pos_ == "NOUN")):
            subjects.append(sentence[token.i+1].text + " " + sentence[token.i+2].text)

    subjectssent = ' '.join(subjects)
    return subjectssent


def getobject(sentence):
    if type(sentence) == str:
        sentence = nlp(sentence)
    objects = []
    tags = ["ART", "DET", "PIAT", "CARD"]
    rootpos = getrootposition(sentence)
    for token in sentence:
        if (((token.dep_ in OBJECTDEP and token.pos_ != "VERB" and token.pos_ != "AUX" ) or (token.pos_ == "NOUN" or token.tag_ == "FM" )) )  :
            if sentence[token.i-1].tag_ in tags:
                objects.append(sentence[token.i-1].text+ " " +token.text)
            # elif sentence[token.i-1].tag_ == "CARD" and sentence[token.i-2].tag_ == "APPR":
            #    objects.append ( sentence[token.i - 2].text + " "+sentence[token.i - 1].text + " " + token.text )
            else:
                objects.append(token.text)
    subjectssent = ' '.join(objects)

    return subjectssent


def getprepostion(sentence):
    prepostions = []
    for token in sentence:
        if token.pos_ == "ADP":
            prepostions.append(token.text)
    return prepostions


def getverbs(sentence):
    if type(sentence) == str:
        sentence = nlp(sentence)
    verbs = []
    for token in sentence:
        if token.pos_ == "VERB" or token.pos_ == "AUX" and token.text == getroot(sentence) :
            verbs.append(token.text)
        if token.tag_ == "PTKVZ" and token.head in verbs and token.text == getroot(sentence) :
            verbs[verbs.index(token.head)] = token.text + " " + token.head.text
    subjectssent = ' '.join(verbs)
    return subjectssent


def tokenizesentence(paragraph):
    sentences = []
    for sent in paragraph.sents:
        sentences.append([sent.text.strip()])
    return sentences


def removepoint(sentence):
    for i, char in enumerate(sentence):
        if i != 0:
            if char == "." and sentence[i-1].isdigit():
                sentence = sentence[:i] + " " + sentence[i + 1:]
    sentence = " ".join (sentence.split())
    return sentence


def SPO(sentence):
    for token in sentence:
        (token.text, token.dep_, token.pos_, token.tag_)


def getsentencesplit(type, sent1, sent2):
    # split sentence containing conjuction
    if type == "OBJ":
        for i, token in enumerate(sent1):
            if (token.pos_ == "VERB" or token.pos_ == "AUX") and token.head is token:
                sent2 = sent1[0: i+1].text + " " + sent2.text
        return sent1,sent2
    elif type == "SBJ":
        for i, token in enumerate ( sent2 ):
            if (token.pos_ == "VERB" or token.pos_ == "AUX") and token.head is token:
                sent1 = sent1.text + " " + sent2[i:].text
        return sent1, sent2
    elif type == "VRB":
        subj = getsubject(sent1)[0]
        for i in sent1:
            if i.text == subj:
                if i.text != i.left_edge.text:
                    subject = i.left_edge.text + " " + i.text + " "
                else:
                    subject= i.text+ " "
        sent2 = subject + sent2.text
        return sent1, sent2


def hasconj(sentence):
    conj = ["und", "aber", "oder", "sondern", "doch"]
    if type(sentence) == str:
        sentence = nlp(sentence)
    result = False
    for i, token in enumerate(sentence):
        if i != 0:
            if token.text in conj:
                result = True
    return result


def hassub(sentence):
    sub = ["weil","während","deshalb","wenn","wann","dass"]
    result = False
    for i, token in enumerate(sentence):
        if i != 0:
            if token.text in sub:
                result = True
    return result

resultsplitund = []


def splitund(sentence):
    conj = ["und", "aber", "oder"]
    sentence = nlp(sentence)
    sent = sentence.text
    objc = getobject(sentence)
    for i, token in enumerate(sentence):
        if i != 0:
            if token.text in conj and sentence[i+1].pos_ == "VERB":
                sent = getsentencesplit("VRB", sentence[:i], sentence[i + 1:])
            elif token.text in conj and sentence[i+1].pos_ != "VERB" and (sentence[i+1].dep_ == "sb" or sentence[i+2].dep_ == "sb") and ( sentence[i+1].pos_ == "NOUN" or sentence[i+1].pos_ == "DET"  or sentence[i+1].pos_ == "PRON"):
                sent = getsentencesplit ("SBJ", sentence[:i], sentence[i + 1:])
            elif token.text in conj and sentence[i+1].pos_ != "VERB" and (sentence[i+1].pos_ == "NOUN" or sentence[i+1].pos_ == "DET"  or sentence[i+1].pos_ == "PRON"):
                sent = getsentencesplit ("OBJ",sentence[:i], sentence[i + 1:])
            else:
                pass
    resultsplitund.append(sent[1])
    if hasconj(sent[0]):
        splitund(sent[0])
    else:
        resultsplitund.append(sent[0])

    return resultsplitund

# here load text to paragraph
my_file = open("text.txt")
list_sentences = tools.loadfile(my_file)
paragraph = ' '.join(list_sentences)
print(paragraph)
quit()
paragraph = removepoint(paragraph)
paragraph = nlp(paragraph)


def pos_tag(sentence):
    pos = {}
    for token in sentence:
        pos.update({token : token.dep_})
    return pos


def touch(fname, times=None):
    with open(fname, 'a'):
        os.utime(fname, times)

tf = "spo.txt"


def generateRDF(sentence, file):
    myfile = Path(file)
    if not (myfile.is_file()):
        touch(file)
    if (myfile.is_file()) and (os.stat(file).st_size == 0):
        rdf = open(file, "w")
        print("## file opened ##")
        print("## writing header ##")
        rdf.write('<?xml version="1.0"?>\n'
                  '<rdf:RDF>\n'
                  '  <Description about="Text Mining project WS16/17 UNI PASSAU">\n'
                  '    <author>Abdesslem Maktouf</author>\n'
                  '    <email>maktouf.abdessalem@gmail.com</email>\n'
                  '    <author>Amani Ben Hadj Hassen</author>\n'
                  '    <email>amanibelhajhsan@gmail.com</email>\n '
                  '</Description>\n'
                  '\n')
        print("## finish writing header ## ")
        rdf.close()
    if (myfile.is_file()) and (os.stat(file).st_size > 0) :
        rdf = open (file)
        lines = rdf.readlines ( )
        rdf.close()
        rdf = open(file, "w")
        rdf.writelines ( [item for item in lines[:-1]] )
        print("## removing footer ## ")
        rdf.close()
        rdf = open (file, "a")
        print("## writing SPO ##")
        rdf.write(
            '  <rdf:SPO '
            '       sentence= '+ sentence + ' >\n'
            '    <subject> '+ getsubject(sentence) +'</subject>\n'
            '    <verbs> '+ getverbs(sentence) +' </verbs>\n'
            '    <object> '+ getobject(sentence) +' </object>\n'
            '  </rdf:SPO>\n'
        )
        print("## finish writing SPO ##")
        rdf.close()
    if (myfile.is_file()) and (os.stat(file).st_size > 0) :
        rdf = open (file, "a")
        print("## writing footer ##")
        rdf.write('</rdf:RDF>\n')
        rdf.close()
        print("## finish writing footer ## ")


for i in tokenizesentence(paragraph):
    sent = nlp(i[0])
    if hassub(sent):
        mainsent = splitsentence(sent)['clauses']['main']
        secondsent = splitsentence(sent)['clauses']['second']
        if hasconj(mainsent):
            for k in splitund(mainsent):
                if type(k) == str:
                    generateRDF(k, tf)
                else:
                    generateRDF(k.text, tf)
        else:
            if type(mainsent) == str:
                generateRDF(mainsent, tf)
            else:
                generateRDF(mainsent.text, tf)
        if hasconj(secondsent):
            for k in splitund(secondsent):
                if type ( k ) == str:
                    generateRDF(k, tf)
                else:
                    generateRDF(k.text, tf)
        else:
            if type(secondsent) == str:
                generateRDF(secondsent, tf)
            else:
                generateRDF(secondsent.text, tf)
    elif hasconj(sent.text):
        for k in splitund(sent):
            if type(k) == str:
                generateRDF(k, tf)
            else:
                generateRDF(k.text, tf)
    else:
        print(sent)
