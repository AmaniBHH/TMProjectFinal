import os
import re


def loadfile(my_file):
    lines = []
    for my_line in my_file:
        lines.append(my_line)
    return lines

# generate the path to the file to be treated
script_dir = os.path.dirname(__file__)    # <-- absolute dir the script is in
rel_path = "DBpediaFiles/redirects_de.ttl"
abs_file_path = os.path.join(script_dir, rel_path)

# create a file to save the urls in
redirects_file = open('DBpediaFiles/redirects.csv', 'w')

with open(abs_file_path, 'r') as f:    # open the file
    contents = loadfile(f)    # put the lines to a variable.
    length = len(contents)

    for index in range(1, length):
        line = contents[index].split()
        my_url = line[0]
        my_list = my_url.split('/')
        my_list[len(my_list) - 1] = re.sub('[<>]', '', my_list[len(my_list) - 1])
        my_id = re.sub('[_]', ' ', my_list[len(my_list) - 1])
        redirects_file.write(my_id)
        redirects_file.write(';')
        redirects_file.write(line[2])
        redirects_file.write('\n')
