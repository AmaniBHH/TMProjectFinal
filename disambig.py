import tools
import csv

# perform a DBpedia search and find related articles about the names mentioned in the article
my_article = []
with open('exp_article2', 'r') as file:  # open the file
    contents = tools.loadfile(file)  # put the lines to a variable.
    for line in contents:
        sentences = tools.tokenize(line)
        for item in sentences:
            my_article.append(item)

urls = dict()
with open('DBpediaFiles/urls.csv', newline='') as csvfile:
    url_reader = csv.reader(csvfile, delimiter=';', quotechar='|')
    for row in url_reader:
        urls[row[0]] = row[1]

# ner tags with their respective url, my_ner_tags = [] of name, type of NE, and url if it exists
my_ner_tags = []
for elem in my_article:

    my_tag = tools.ner_sent1(elem)
    if my_tag:
        for i in range(0, len(my_tag)):
            url = ""
            """print("**************************", i, "**************************")
            print(my_tag)
            print(my_tag[i])"""
            sub = my_tag[i][0]
            # print("sub :", sub)
            if sub in urls:
                url = urls[sub]
            # print(sub, url)
            if url:
                my_ner_tags.append([my_tag[i][0], my_tag[i][1], url])


# save results
output = open("disambig_art.ttl", "w")

for elem in my_ner_tags:
    output.write("<" + elem[0] + "> <" + elem[1] + "> " + elem[2] + ".\n")


# save results, method 2
rdf = open("rdf_disambig.txt", "w")
rdf.write('<?xml version="1.0"?>\n'
                  '<rdf:RDF>\n'
                  '  <Description about="Text Mining project WS16/17 UNI PASSAU">\n'
                  '    <author>Abdesslem Maktouf</author>\n'
                  '    <email>maktouf.abdessalem@gmail.com</email>\n'
                  '    <author>Amani Ben Hadj Hassen</author>\n'
                  '    <email>amanibelhajhsan@gmail.com</email>\n '
                  '</Description>\n'
                  '\n')
for elem in my_ner_tags:
    rdf.write(
        '  <rdf:SPO '
        '    <subject> ' + elem[0] + '</subject>\n'
        '    <type> ' + elem[1] + ' </type>\n'
        '    <url> ' + elem[2] + ' </url>\n'
        '  </rdf:SPO>\n'
    )
rdf.write('</rdf:RDF>\n')
print("***** The end ! *****")

