import spacy

nlp = spacy.load('de')
# nlp_en = spacy.load('en')


def loadfile(my_file):
    lines = []
    for line in my_file:
        lines.append(line)
    return lines


def my_nlp(sentences):
    list = []
    for sentence in sentences:
        list.append(nlp(sentence))
    return list


def find(sentence, word):
    """ returns the first occurence of a word in a paragraph """
    index = -1
    for token in sentence:
        # print(token)
        if token.text == word.text:
            return token.i
    return index


def find2(paragraph, word):
    """ returns the first occurence of a word in a paragraph. paragraph is a list of sentences """
    index = -1
    for token in paragraph:
        # print(token)
        if token.text == word.text:
            return token.i
    return index


def find_pronoun(paragraph, pronoun):
    """ returns indices of a certain pronoun in a paragraph. paragraph is a list of sentences """
    indices = []
    i = 0
    for sent in paragraph:
        nlp_sentence = nlp(sent)
        for token in nlp_sentence:
            # print(token)
            if token.text.lower() == pronoun:
                indices.append(i)
            i += 1
    return indices


def tokenize(sent):
    tokens = []
    doc1 = nlp(sent)
    for token in doc1:
        tokens.append(token.orth_)
    return tokens


def dependency(sent):
    doc1 = nlp(sent)
    for dep in doc1:
        print(dep, dep.dep_, dep.head)


def tokenize_paragraph(paragraph):
    paragraph = nlp(paragraph)
    sentences = []
    for sent in paragraph.sents:
        sentences.append([sent])
    return sentences



def lemmatize(sentence):
    sentence = nlp(sentence)
    dictio = {}
    for token in sentence:
        dictio.update({token: token.lemma_})
    return dictio


def tok_format(tok):
    return " ".join([tok.dep_, tok.orth_])


def to_nltk_tree(node):
    if (node.n_lefts + node.n_rights > 0):
        return (Tree(tok_format(node), [to_nltk_tree(child) for child in node.children]))
    else:
        return tok_format(node)


def c_tree(sentence):
    sentence = nlp(sentence)
    for sent in sentence.sents:
        print(to_nltk_tree(sent.root))


def pos_tag(sentence):
    sentence = nlp(sentence)
    pos = {}
    for token in sentence:
        pos.update({token: token.pos_})
    return pos


def get_subjectpos(sentence):
    sentencenlp = nlp(sentence)
    for token in sentencenlp:
        if token.dep_ == "sb":
            pos = sentence.index(token.string)
    return pos


def ner_sent1(sentence):
    sentence = nlp(sentence)
    ner = []
    for ent in sentence.ents:
        ner.append([ent.text, ent.label_])
    return ner

def ner_sent(sentence, paragraph):
    sentence = nlp(sentence)
    ner = []
    for ent in sentence.ents:
        i = find(sentence, ent)
        ner.append([ent, ent.label_, i])
    return ner


def ner_paragraph(paragraph):
    # sentences = paragraph.split('.')
    # print(sentences)
    ners = []
    for sentence in paragraph:
        elems = ner_sent(sentence, paragraph)
        for elem in elems:
            if elem != []:
                ners.append(elem)
    return ners


def NounChunk(sentence):
    sentence = nlp(sentence)
    nouns = {}
    key = 0
    for np in sentence.noun_chunks:
        nouns.update({key: np})
        key += 1
    return nouns


def get_subject(sentence):
    sentencenlp = nlp(sentence)
    sbj_tag = ['sb']
    subjects = []
    for token in sentencenlp:
        if (token.tag_ in sbj_tag):
            subjects.append(token)

    return subjects


def test(sentence):
    sentencenlp = nlp(sentence)
    verb_tag = ["VVFIN", "VVIMP", "VVPP", "VAFIN", "VAIMP", "VAPP", "VMFIN", "VMPP"]
    verbinsentence = []
    sentences = []
    for token in sentencenlp:
        if (token.tag_ in verb_tag):
            verbinsentence.append([token, token.tag_])
    if len(verbinsentence) > 1:
        words = tokenize(sentence)
        keyword = ","
        if (keyword in words):
            indexofword = words.index(keyword)
            if (sentencenlp[indexofword + 1].tag_ == "KOUS") and (
                        sentencenlp[len(sentencenlp) - 2].tag_ in verb_tag):
                sentences.append(sentencenlp[0:indexofword])
                print("here is the subject ", get_subjectpos(sentence[indexofword + 2:]), "stop")
                sentences.append(sentencenlp[indexofword + 2:])
            if (sentencenlp[indexofword + 1].tag_ == "KOUS") and (
                        sentencenlp[len(sentencenlp) - 1].tag_ in verb_tag):
                sentences.append(sentencenlp[0:indexofword])
                sentences.append(sentencenlp[indexofword + 2:])
        if len(sentences) > 0:
            return sentences
        else:
            return sentence
    else:
        return sentence
