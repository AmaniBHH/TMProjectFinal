import tools
import nltk
# Corpus which consists of male and female names dataset
from nltk.corpus import names
# For shuffling
import random


# Gender classifier
def gender_features(word):
    return {'last_letter': word[-1]}


def gender_classifier():
    labeled_names = ([(name, 'male') for name in names.words('male.txt')] +
         [(name, 'female') for name in names.words('female.txt')])
    random.shuffle(labeled_names)
    featuresets = [(gender_features(n), gender) for (n, gender) in labeled_names]
    train_set, test_set = featuresets[500:], featuresets[:500]
    classifier = nltk.NaiveBayesClassifier.train(train_set)
    return classifier


def coreference_resolution(my_file):
    my_sentences = tools.loadfile(my_file)

    my_ne = tools.ner_paragraph(my_sentences)

    """ extract pronouns from text : er and sie """
    # er
    pronouns_er = tools.find_pronoun(my_sentences, "er")
    print(pronouns_er)
    # sie
    pronouns_sie = tools.find_pronoun(my_sentences, "sie")
    print(pronouns_sie)

    classifier = gender_classifier()
    my_ne2 = []
    for elem in my_ne:
        if elem[1] == 'PERSON':
            gen = classifier.classify(gender_features(elem[0].text))
            my_ne2.append([elem[0], elem[2], gen])

    # print(my_ne2)
    coref = []
    for element in my_ne2:
        for pronoun in pronouns_er:
            min = 1000
            index = -1
            name = ""
            if element[2] == 'male':
                dist = abs(element[1] - pronoun)
                if dist < min:
                    dist = min
                    [index, name] = [pronoun, element[0].text]

            if name != "" and index != -1:
                coref.append([name, index])

        for pronoun in pronouns_sie:
            min = 1000
            name = ""
            index = -1
            if element[2] == 'female':
                dist = abs(element[1] - pronoun)
                if dist < min:
                    dist = min
                    [index, name] = [pronoun, element[0].text]
            if name != "" and index != -1:
                coref.append([name, index])

    return coref

